package fr.erdf.siae.mockito;

import fr.erdf.siae.mockito.matchers.Matcher;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

public class Invocation<T> {

	Method method;
	Object[] params;

	private BiFunction<Object, Invocation<T>, T> res;

	Invocation(Method method, Object[] params, BiFunction<Object, Invocation<T>, T> res, List<Matcher> matchers) {
		this.method = method;
		this.params = params;
		this.res = res;
		for (int i = 0; i < matchers.size(); i++) {
			params[i] = matchers.get(i);
		}
	}

	Invocation(Method method, Object[] params) {
		this(method, params, (instance, invocation) -> null, Collections.emptyList());
	}

	Invocation(BiFunction<Object, Invocation<T>, T> res) {
		this(null, null, res, Collections.emptyList());
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (other == null || getClass() != other.getClass()) return false;
		Invocation<?> that = (Invocation<?>) other;
		return Objects.equals(method, that.method) && Arrays.equals(params, that.params);

	}

	@Override
	public int hashCode() {
		int result = method != null ? method.hashCode() : 0;
		result = 31 * result + Arrays.hashCode(params);
		return result;
	}

	<U> T call(U instance) {
		return res.apply(instance, this);
	}
}
