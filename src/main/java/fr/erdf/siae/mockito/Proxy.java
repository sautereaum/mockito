package fr.erdf.siae.mockito;

import fr.erdf.siae.mockito.exceptions.MatcherError;
import fr.erdf.siae.mockito.exceptions.MockInstantiationException;
import fr.erdf.siae.mockito.exceptions.UnfinishedStubInvocation;
import fr.erdf.siae.mockito.matchers.Matcher;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.BiFunction;

@SuppressWarnings("unchecked")
class Proxy<T> implements MethodInterceptor {

	private static Map<Object, LinkedList<Invocation>> stubs = new HashMap<>();
	private static BiFunction<Object, Invocation<Object>, Object> callBackOnInvocation;
	private static List<Matcher> matchers = new LinkedList<>();

	private T instance;
	private Answer.defaultAnswer defaultAnswer;

	private Proxy(T instance, Answer.defaultAnswer defaultAnswer) {
		this.instance = instance;
		this.defaultAnswer = defaultAnswer;
	}

	private Proxy(Answer.defaultAnswer defaultAnswer) {
		this.defaultAnswer = defaultAnswer;
	}

	private static <T> T initialize(Proxy<T> proxy, Class<?> superClass) {
		Enhancer e = new Enhancer();
		if (superClass.isInterface()) {
			e.setInterfaces(new Class[]{MockInterface.class, superClass});
		} else {
			e.setSuperclass(superClass);
			e.setInterfaces(new Class[]{MockInterface.class});
		}
		e.setCallback(proxy);
		return (T) e.create();
	}

	static <T> T newInstance(T instance, Answer.defaultAnswer defaultAnswer) {
		Proxy<T> proxy = new Proxy<>(instance, defaultAnswer);
		return initialize(proxy, proxy.instance.getClass());
	}

	static <T> T newInstance(Class<T> clazz, Answer.defaultAnswer defaultAnswer) {
		return initialize(new Proxy<>(defaultAnswer), clazz);
	}

	static <T> void registerNextInvocation(BiFunction<Object, Invocation<Object>, T> res) {
		if (callBackOnInvocation != null) {
			callBackOnInvocation = null;
			throw new UnfinishedStubInvocation();
		}
		matchers = new LinkedList<>();
		callBackOnInvocation = (BiFunction<Object, Invocation<Object>, Object>) res;
	}

	static void registerMatcher(Matcher matcher) {
		matchers.add(matcher);
	}

	static <T> Object doCallRealMethod(T instance, Method method, Object[] params) {
		if (instance == null) {
			throw new MockInstantiationException("This mock has no self-instance. Field injection is not possible.");
		}
		try {
			return method.getDeclaringClass()
						 .getMethod(method.getName(), Arrays.stream(params)
						 .map(Object::getClass)
						 .toArray(Class[]::new))
						 .invoke(instance, params);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}

	private void injectFields(Object proxyInstance) {
		if (instance == null) {
			throw new MockInstantiationException("This mock has no self-instance. Field injection is not possible.");
		}
		Arrays.stream(proxyInstance.getClass().getSuperclass().getDeclaredFields()).filter(e -> {
			try {
				if (!e.isAccessible()) {
					e.setAccessible(true);
				}
				return e.get(proxyInstance) != null;
			} catch (IllegalAccessException e1) {
				return false;
			}
		}).forEach(e -> {
			try {
				e.set(instance, e.get(proxyInstance));
			} catch (IllegalAccessException e1) {
				e1.printStackTrace();
			}
		});
	}

	private void registerMockedInvocation(Object proxyInstance, Method method, Object[] params) {
		if (matchers.size() != 0 && matchers.size() != params.length) {
			int got = matchers.size();
			throw new MatcherError(params.length, got);
		}
		stubs.compute(proxyInstance, (key, value) -> {
			if (value == null) {
				value = new LinkedList<>();
			}
			value.addFirst(new Invocation<>(method, params, callBackOnInvocation, new LinkedList<>(matchers)));
			return value;
		});
	}

	private Object computeStubbedInvocation(Object proxyInstance, Method method, Object[] params) {
		BiFunction<Object, Invocation<Object>, Object> res = (instance, inv) -> {
			if (defaultAnswer == Answer.defaultAnswer.CALL_REAL_METHOD) {
				return doCallRealMethod(instance, method, params);
			}
			return null;
		};
		if (!stubs.containsKey(proxyInstance)) {
			return res.apply(instance, null);
		}
		return stubs.get(proxyInstance)
				.stream()
				.filter(e -> e.equals(new Invocation(method, params)))
				.findFirst()
				.orElse(new Invocation(res))
				.call(instance);
	}

	public Object intercept(Object proxyInstance, Method method, Object[] params, MethodProxy methodProxy) throws Throwable {
		injectFields(proxyInstance);
		if (method.getDeclaringClass().equals(Object.class)) {
			return doCallRealMethod(instance == null ? "null" : instance, method, params);
		}
		if (callBackOnInvocation != null) {
			registerMockedInvocation(proxyInstance, method, params);
			callBackOnInvocation = null;
			return null;
		}
		try {
			return computeStubbedInvocation(proxyInstance, method, params);
		} catch (RuntimeException e) {
			throw new Throwable(e);
		}
	}

}
