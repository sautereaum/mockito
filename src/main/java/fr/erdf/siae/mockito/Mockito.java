package fr.erdf.siae.mockito;

import fr.erdf.siae.mockito.exceptions.MockInstantiationException;
import fr.erdf.siae.mockito.matchers.Any;
import fr.erdf.siae.mockito.matchers.Eq;

@SuppressWarnings("unchecked")
public class Mockito {

	private Mockito() {
	}

	public static <T> T mock(Class<T> clazz) {
		return mock(clazz, Answer.defaultAnswer.RETURN_NULL);
	}

	public static <T> T mock(Class<T> clazz, Answer.defaultAnswer defaultAnswer) {
		try {
			return Proxy.newInstance(clazz.newInstance(), defaultAnswer);
		} catch (InstantiationException | IllegalAccessException e) {
			System.err.println("Mock instantiation failed, try without self instance");
			e.printStackTrace();
		}
		try {
			return Proxy.newInstance(clazz, defaultAnswer);
		} catch (Throwable t) {
			throw new MockInstantiationException(t);
		}
	}

	public static <T> T spy(T instance) {
		try {
			return Proxy.newInstance(instance, Answer.defaultAnswer.CALL_REAL_METHOD);
		} catch (Throwable t) {
			throw new MockInstantiationException(t);
		}
	}

	public static <T> T spy(Class<T> clazz) {
		return mock(clazz, Answer.defaultAnswer.CALL_REAL_METHOD);
	}

	public static <T> Answer<T> doReturn(T res) {
		return new Answer<>(res);
	}

	public static Answer<Object> doNothing() {
		return new Answer<>(new Object());
	}

	public static Answer<Object> doCallRealMethod() {
		return new Answer<>((Object instance, Invocation<Object> invocation) ->
				Proxy.doCallRealMethod(instance, invocation.method, invocation.params)
		);
	}

	public static <T> T any() {
		Proxy.registerMatcher(Any.getInstance());
		return null;
	}

	public static <T> T any(Class<T> clazz) {
		Proxy.registerMatcher(new Any<>(clazz));
		return null;
	}

	public static int anyInt() {
		Proxy.registerMatcher(Any.getInstance());
		return 0;
	}

	public static <T> T eq(T value) {
		Proxy.registerMatcher(new Eq(value));
		return null;
	}

}
