package fr.erdf.siae.mockito.exceptions;

public class NotAMockException extends RuntimeException {

	public NotAMockException(Object object) {
		super("Mockito can't mock methods of " + object + " witch is a non-mock object.");
	}

}
