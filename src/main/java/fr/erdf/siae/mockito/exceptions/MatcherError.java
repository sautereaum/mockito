package fr.erdf.siae.mockito.exceptions;

public class MatcherError extends RuntimeException {

	public MatcherError(int wanted, int got) {
		super("expected 0 or " + wanted + " matchers but got " + got + ".");
	}
}
