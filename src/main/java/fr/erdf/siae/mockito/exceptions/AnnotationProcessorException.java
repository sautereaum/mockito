package fr.erdf.siae.mockito.exceptions;

public class AnnotationProcessorException extends RuntimeException {

	public AnnotationProcessorException(String message) {
		super(message);
	}

	public AnnotationProcessorException(String message, Throwable cause) {
		super(message, cause);
	}
}
