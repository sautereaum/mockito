package fr.erdf.siae.mockito.exceptions;

public class MockInstantiationException extends RuntimeException {

	public MockInstantiationException(String message) {
		super(message);
	}

	public MockInstantiationException(Throwable throwable) {
		super("Something goes wrong when initialize mock", throwable);
	}

}
