package fr.erdf.siae.mockito;

import fr.erdf.siae.mockito.exceptions.NotAMockException;

import java.util.Arrays;
import java.util.function.BiFunction;

public class Answer<T> {

	public enum defaultAnswer {
		RETURN_NULL,
		CALL_REAL_METHOD
	}

	private BiFunction<Object, Invocation<Object>, T> res;

	Answer(T res) {
		this((instance, invocation) -> res);
	}

	Answer(BiFunction<Object, Invocation<Object>, T> res) {
		this.res = res;
	}

	public <U> U when(U mock) {
		if (Arrays.stream(mock.getClass().getInterfaces()).noneMatch(e -> e.equals(MockInterface.class))) {
			throw new NotAMockException(mock);
		}
		Proxy.registerNextInvocation(res);
		return mock;
	}
}
