package fr.erdf.siae.mockito.matchers;

public abstract class Matcher {

	Matcher() {

	}

	public abstract boolean equals(Object other);
}
