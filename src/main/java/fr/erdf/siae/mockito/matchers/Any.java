package fr.erdf.siae.mockito.matchers;

@SuppressWarnings("unchecked")
public class Any<T> extends Matcher {

	private static Any instance;

	private Class<T> clazz;

	private Any() {
		this((Class<T>) Object.class);
	}

	public Any(Class<T> clazz) {
		this.clazz = clazz;
	}

	public static Any getInstance() {
		return instance != null ? instance : (instance = new Any());
	}

	@Override
	public boolean equals(Object other) {
		try {
			clazz.cast(other);
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}
}
