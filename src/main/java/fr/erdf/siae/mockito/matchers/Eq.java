package fr.erdf.siae.mockito.matchers;

public class Eq<T> extends Matcher {

	private T value;

	public Eq(T value) {
		this.value = value;
	}

	@Override
	@SuppressWarnings({"EqualsWhichDoesntCheckParameterClass"})
	public boolean equals(Object other) {
		return other.equals(value);
	}
}
