package fr.erdf.siae.mockito.annotation;

import fr.erdf.siae.mockito.exceptions.AnnotationProcessorException;

import java.lang.reflect.Field;
import java.util.Arrays;

import static fr.erdf.siae.mockito.Mockito.mock;
import static fr.erdf.siae.mockito.Mockito.spy;

public class AnnotationProcessor {

	private AnnotationProcessor() {
	}

	public static void initAnnotation(Object obj) {
		verifyAnnotation(obj);
		initMockAnnotation(obj);
		initSpyAnnotation(obj);
		initInjectMockAnnotation(obj);
	}

	private static void initInjectMockAnnotation(Object obj) {
		Arrays.stream(obj.getClass().getDeclaredFields()).filter(e -> e.getAnnotation(InjectMock.class) != null).forEach(e -> {
			if (!e.isAccessible()) {
				e.setAccessible(true);
			}
			for (Field field : e.getType().getDeclaredFields()) {
				if (!field.isAccessible()) {
					field.setAccessible(true);
				}
				try {
					if (e.get(obj) != null && field.get(e.get(obj)) == null) {
						Field objField = Arrays.stream(obj.getClass()
											   .getDeclaredFields())
											   .filter(
											   		f -> e.getType().equals(field.getType()) &&
														 e.getName().equals(field.getName()))
											   .findFirst()
											   .orElse(null);
						if (objField != null && !objField.isAccessible()) {
							objField.setAccessible(true);
						}
						field.set(e.get(obj), objField != null ? objField.get(obj) : mock(field.getType()));
					}
				} catch (IllegalAccessException e1) {
					throw new AnnotationProcessorException("Something goes wrong when processed @InjectMock annotation", e1);
				}
			}
		});
	}

	private static void verifyAnnotation(Object obj) {
		if (Arrays.stream(obj.getClass().getDeclaredFields()).anyMatch(e -> e.getAnnotation(Mock.class) != null && e.getAnnotation(Spy.class) != null)) {
			throw new AnnotationProcessorException("Field should not be annoted with @Mock and @Spy simultaneously");
		}
	}

	private static void initMockAnnotation(Object obj) {
		Arrays.stream(obj.getClass().getDeclaredFields()).filter(e -> e.getAnnotation(Mock.class) != null).forEach(e -> {
			if (!e.isAccessible()) {
				e.setAccessible(true);
			}
			try {
				e.set(obj, mock(e.getType(), e.getAnnotation(Mock.class).defaultAnswer()));
			} catch (IllegalAccessException e1) {
				throw new AnnotationProcessorException("Something goes wrong when processed @Mock annotation", e1);
			}
		});
	}

	private static void initSpyAnnotation(Object obj) {
		Arrays.stream(obj.getClass().getDeclaredFields()).filter(e -> e.getAnnotation(Spy.class) != null).forEach(e -> {
			if (!e.isAccessible()) {
				e.setAccessible(true);
			}
			try {
				Object instance = e.get(obj);
				if (instance != null) {
					e.set(obj, spy(e.get(obj)));
				} else {
					e.set(obj, spy(e.getType()));
				}
			} catch (IllegalAccessException e1) {
				throw new AnnotationProcessorException("Something goes wrong when processed @Spy annotation", e1);
			}
		});
	}

}
