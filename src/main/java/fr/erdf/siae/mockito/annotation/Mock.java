package fr.erdf.siae.mockito.annotation;

import fr.erdf.siae.mockito.Answer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Mock {

	Answer.defaultAnswer defaultAnswer() default Answer.defaultAnswer.RETURN_NULL;

}
