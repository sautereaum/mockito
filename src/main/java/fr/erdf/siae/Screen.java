package fr.erdf.siae;

public class Screen {

	private String text;

	public Screen() {
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
