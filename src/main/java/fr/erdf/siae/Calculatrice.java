package fr.erdf.siae;

public class Calculatrice {

	private Screen screen;

	public Calculatrice() {
	}

	public Calculatrice(Screen screen) {
		this.screen = screen;
	}

	public Integer sum(Integer a, Integer b) {
		return a + b;
	}

	public int sumPrimitive(int a, int b) {
		return a + b;
	}
	
	public Integer substract(Integer a, Integer b) {
		return a - b;
	}

	public Screen getScreen() {
		return screen;
	}
	
}
