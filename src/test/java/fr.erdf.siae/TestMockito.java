package fr.erdf.siae;

import fr.erdf.siae.mockito.Answer;
import fr.erdf.siae.mockito.annotation.AnnotationProcessor;
import fr.erdf.siae.mockito.annotation.InjectMock;
import fr.erdf.siae.mockito.annotation.Mock;
import fr.erdf.siae.mockito.annotation.Spy;
import fr.erdf.siae.mockito.exceptions.MatcherError;
import fr.erdf.siae.mockito.exceptions.UnfinishedStubInvocation;
import org.junit.Before;
import org.junit.Test;

import static fr.erdf.siae.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@SuppressWarnings({"ResultOfMethodCallIgnored", "unused"})
public class TestMockito {

	@Mock()
	private Calculatrice mockedCalculatrice;

	@Spy()
	private Calculatrice spyedCalculatrice = new Calculatrice();

	@Mock(defaultAnswer = Answer.defaultAnswer.CALL_REAL_METHOD)
	private Calculatrice spyedAsMockCalculatrice;

	@Spy()
	@InjectMock()
	private Calculatrice injectedCalculatrice;

	@Mock()
	private Screen screen;

	@Before()
	public void before() {
		AnnotationProcessor.initAnnotation(this);
	}

	@Test()
	public void testNormal() {
		Calculatrice calculatrice = new Calculatrice();
		assertEquals(Integer.valueOf(3), calculatrice.substract(5, 2));
	}

	@Test()
	public void testWithMock() {
		Calculatrice calculatrice = mock(Calculatrice.class);
		assertNull(calculatrice.substract(5, 2));
	}

	@Test()
	public void testWithMockAndWhen() {
		Calculatrice calculatrice = mock(Calculatrice.class);
		doReturn(10).when(calculatrice).sum(3, 5);
		assertNull(calculatrice.sum(5, 2));
		assertEquals(Integer.valueOf(10), calculatrice.sum(3, 5));
	}

	@Test
	public void testWithCallRealMethod() {
		Calculatrice calculatrice = mock(Calculatrice.class);

		doReturn(10).when(calculatrice).sum(3, 5);
		doCallRealMethod().when(calculatrice).sum(1, 1);
		assertNull(calculatrice.sum(5, 2));
		assertEquals(Integer.valueOf(10), calculatrice.sum(3, 5));
		assertEquals(Integer.valueOf(2), calculatrice.sum(1, 1));
	}

	@Test
	public void testWithAnyMatcher() {
		Calculatrice calculatrice = mock(Calculatrice.class);

		doReturn(5).when(calculatrice).sum(any(), any());

		assertEquals(Integer.valueOf(5), calculatrice.sum(3, 5));
		assertEquals(Integer.valueOf(5), calculatrice.sum(1, 1));
	}

	@Test(expected = NullPointerException.class)
	public void testWithPrimitiveAnyObject() {
		Calculatrice calculatrice = mock(Calculatrice.class);
		doReturn(3).when(calculatrice).sumPrimitive(any(), any());
		assertEquals(3, calculatrice.sumPrimitive(3, 5));
	}

	@Test
	public void testWithPrimitiveAnyInt() {
		Calculatrice calculatrice = mock(Calculatrice.class);
		doReturn(3).when(calculatrice).sumPrimitive(anyInt(), anyInt());
		assertEquals(3, calculatrice.sumPrimitive(3, 5));
	}

	@Test
	public void testWithOtherMatchers() {
		Calculatrice calculatrice = mock(Calculatrice.class);

		doReturn(5).when(calculatrice).sum(any(), any());
		doReturn(10).when(calculatrice).substract(any(Integer.class), any(Integer.class));
		doReturn(50).when(calculatrice).substract(any(), eq(10));

		assertEquals(Integer.valueOf(5), calculatrice.sum(3, 5));
		assertEquals(Integer.valueOf(5), calculatrice.sum(1, 1));
		assertEquals(Integer.valueOf(10), calculatrice.substract(1, 1));
		assertEquals(Integer.valueOf(50), calculatrice.substract(1, 10));
		assertEquals(Integer.valueOf(50), calculatrice.substract(100, 10));
		assertEquals(Integer.valueOf(10), calculatrice.substract(100, 11));
	}

	@Test
	public void testWithSpy() {
		Calculatrice calculatrice = spy(new Calculatrice());

		assertEquals(Integer.valueOf(10), calculatrice.sum(3, 7));

		doReturn(5).when(calculatrice).sum(any(), any());
		doReturn(10).when(calculatrice).substract(any(Integer.class), any(Integer.class));
		doReturn(50).when(calculatrice).substract(any(), eq(10));

		assertEquals(Integer.valueOf(5), calculatrice.sum(3, 5));
		assertEquals(Integer.valueOf(5), calculatrice.sum(1, 1));
		assertEquals(Integer.valueOf(10), calculatrice.substract(1, 1));
		assertEquals(Integer.valueOf(50), calculatrice.substract(1, 10));
		assertEquals(Integer.valueOf(50), calculatrice.substract(100, 10));
		assertEquals(Integer.valueOf(10), calculatrice.substract(100, 11));
	}

	@Test(expected = MatcherError.class)
	public void testStrangeMatcherBehaviour() {
		Calculatrice calculatrice = mock(Calculatrice.class);
		doReturn(5).when(calculatrice).sum(5, any());
		assertNull(calculatrice.sum(5, 5));
	}

	@Test
	public void testWithMockAnnotationOnField() {
		doReturn(5).when(mockedCalculatrice).sum(any(), any());
		assertEquals(Integer.valueOf(5), mockedCalculatrice.sum(1, 2));
	}

	@Test
	public void testWithSpyAnnotationOnField() {
		assertEquals(Integer.valueOf(12), spyedCalculatrice.sum(8, 4));
		doReturn(5).when(spyedCalculatrice).sum(any(), any());
		assertEquals(Integer.valueOf(5), spyedCalculatrice.sum(1, 2));
	}

	@Test
	public void testWithSpyAsMockAnnotationOnField() {
		assertEquals(Integer.valueOf(12), spyedAsMockCalculatrice.sum(8, 4));
		doReturn(5).when(spyedAsMockCalculatrice).sum(any(), any());
		assertEquals(Integer.valueOf(5), spyedAsMockCalculatrice.sum(1, 2));
	}

	@Test
	public void testWithChainedStubs() {
		Screen screen = new Screen();
		screen.setText("calculatrice");
		Calculatrice calculatrice = new Calculatrice(screen);
		assertEquals("calculatrice", calculatrice.getScreen().getText());
		calculatrice = mock(Calculatrice.class);
		screen = mock(Screen.class);
		doReturn(screen).when(calculatrice).getScreen();
		doReturn("super calculatrice").when(screen).getText();
		assertEquals("super calculatrice", calculatrice.getScreen().getText());
	}

	@Test
	public void testWithInjectMock() {
		doNothing().when(screen).setText(any());
		doReturn("super calculatrice").when(injectedCalculatrice.getScreen()).getText();
		assertEquals("super calculatrice", injectedCalculatrice.getScreen().getText());
	}

	@Test(expected = UnfinishedStubInvocation.class)
	public void testUnfinishedStub() {
		doNothing().when(screen);
		doNothing().when(screen);
	}

}
